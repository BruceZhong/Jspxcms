<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fnx" uri="http://java.sun.com/jsp/jstl/functionsx" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="f" uri="http://www.jspxcms.com/tags/form" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/head.jsp" />
</head>
<body class="skin-blue content-body">
<div class="content-header">
    <h1>后台首页</h1>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-edit"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">发布文档（最近7日）</span>
                    <span class="info-box-number">${infos}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">用户注册（最近7日）</span>
                    <span class="info-box-number">${users}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-commenting"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">用户评论（最近7日）</span>
                    <span class="info-box-number">${comments}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-book"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">用户留言（最近7日）</span>
                    <span class="info-box-number">${guestbooks}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">您的资料</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th style="width:30%">当前版本</th>
                            <td>Jspxcms v${site.version}<a href="http://www.jspxcms.com/" target="_blank" class="latest-version">查看最新版</a></td>
                        </tr>
                        <tr>
                            <th>用户名</th>
                            <td>${user.username}</td>
                        </tr>
                        <tr>
                            <th>上次登录时间</th>
                            <td>
                                <c:choose>
                                    <c:when test="${empty user.prevLoginDate}">您是第一次登录</c:when>
                                    <c:otherwise><fmt:formatDate value="${user.prevLoginDate}" pattern="yyyy-MM-dd HH:mm:ss" /></c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th>上次登录IP</th>
                            <td>
                                <c:choose>
                                    <c:when test="${empty user.prevLoginIp}">您是第一次登录</c:when>
                                    <c:otherwise><c:out value="${user.prevLoginIp}" /></c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th>本次登录时间</th>
                            <td><fmt:formatDate value="${user.lastLoginDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                        </tr>
                        <tr>
                            <th>本次登录IP</th>
                            <td><c:out value="${user.lastLoginIp}" /></td>
                        </tr>
                        <tr>
                            <th>登录次数</th>
                            <td><fmt:formatNumber value="${user.logins}" pattern="#,###" /></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>